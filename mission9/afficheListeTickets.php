<?php 
	require("recupTicket.php");
	creatBaseTicket();
	$conn=connectDB();
	//$data1=$query->fetch(PDO::FETCH_ASSOC);
	//echo $data1;
	
		
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>List of tickets</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
		<link rel="stylesheet" href="ticket.css">
		<script src="afficheTickets.js"></script>
</head>
<body>
<div class="container">

    <label for="select1"><h1>List of tickets</h1></label><br>
		<select id="select1" > 
		<option value="">Select a ticket:</option>
	<?php 
	$select="SELECT id,sujet FROM ticket";
	$query=$conn->query($select);
	while($data=$query->fetch(PDO::FETCH_ASSOC))
	{
	
	?>
		  <option value="<?php echo $data["id"];?>"<?php if($data["id"]==$select){ echo "selected";}?>><?php echo $data["sujet"];?></option>
	<?php
		}
	?>
	</select>
	<br>
	<div id="ok"><b>Ticket information will be listed here...</b></div>
	
</div>
</body>
</html>