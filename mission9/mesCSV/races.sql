-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 04, 2022 at 09:58 AM
-- Server version: 5.7.36
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zoo`
--

-- --------------------------------------------------------

--
-- Table structure for table `races`
--

DROP TABLE IF EXISTS `races`;
CREATE TABLE IF NOT EXISTS `races` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom` varchar(60) NOT NULL,
  `type_food` enum('Carnivore','Herbivore','Omnivore') NOT NULL,
  `aquatique` tinyint(1) NOT NULL,
  `duree_vie` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `races`
--

INSERT INTO `races` (`id`, `nom`, `type_food`, `aquatique`, `duree_vie`) VALUES
(1, 'Chien', 'Omnivore', 0, 14),
(2, 'Chat', 'Omnivore', 0, 15),
(3, 'Requin', 'Carnivore', 1, 55),
(4, 'Tortue', 'Herbivore', 0, 60),
(5, 'Ã‰lÃ©phant', 'Herbivore', 0, 60),
(6, 'Dauphin', 'Carnivore', 1, 40),
(7, 'Girafe', 'Herbivore', 0, 15),
(8, 'Ours', 'Omnivore', 0, 30),
(9, 'Loup', 'Carnivore', 0, 14),
(10, 'Pingouin', 'Carnivore', 1, 20);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
