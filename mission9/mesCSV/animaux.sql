-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 04, 2022 at 09:57 AM
-- Server version: 5.7.36
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zoo`
--

-- --------------------------------------------------------

--
-- Table structure for table `animaux`
--

DROP TABLE IF EXISTS `animaux`;
CREATE TABLE IF NOT EXISTS `animaux` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `race` int(10) UNSIGNED NOT NULL,
  `date_nais` date DEFAULT NULL,
  `sexe` enum('M','F') DEFAULT NULL,
  `pseudo` varchar(40) NOT NULL,
  `commentaire` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `animaux`
--

INSERT INTO `animaux` (`id`, `race`, `date_nais`, `sexe`, `pseudo`, `commentaire`) VALUES
(1, 1, '2016-02-23', NULL, 'Canigou', 'Oreille lacÃ©rÃ©e'),
(2, 1, '2002-11-02', 'M', 'Pluto', 'Mordille beaucoup'),
(3, 1, '1995-09-05', 'F', 'Missie', NULL),
(4, 1, '2003-02-27', 'M', 'Waouf', NULL),
(5, 1, '2016-03-17', NULL, 'Bill', 'DerniÃ¨re portÃ©e de Barjotte'),
(6, 1, '2016-03-17', NULL, 'Sed', 'DerniÃ¨re portÃ©e de Barjotte'),
(7, 1, '2003-06-01', 'F', 'Barjotte', NULL),
(8, 1, '2016-03-17', NULL, 'Nonoss', 'DerniÃ¨re portÃ©e de Barjotte'),
(9, 2, '2006-03-09', 'F', 'Miaou', NULL),
(10, 2, '2009-06-27', 'F', 'Minette', NULL),
(11, 2, '2007-12-22', 'M', 'Lourd', 'Aime les calins'),
(12, 2, '2016-03-03', NULL, 'Petio', NULL),
(13, 2, '2000-02-23', 'M', 'Siestas', NULL),
(14, 3, '2016-01-21', NULL, 'Sharky', NULL),
(15, 3, '1983-05-27', 'M', 'Mordeur', 'Il peut Ãªtre dangereux'),
(16, 3, '2016-02-04', NULL, 'BronzÃ©', NULL),
(17, 4, '1950-04-20', 'F', 'Caline', 'Manque une patte'),
(18, 4, '1996-08-21', 'F', 'Justy', 'Carapace endommagÃ©e'),
(19, 4, '2010-03-08', 'F', 'Lizzie', NULL),
(20, 4, '2002-09-10', 'M', 'Lucky', NULL),
(21, 5, '2016-02-20', 'F', 'Bobonne', NULL),
(22, 5, '1953-05-19', 'M', 'Maouss', 'DÃ©fense cassÃ©e'),
(23, 5, '1985-04-23', 'F', 'Matrone', 'Maman de LÃ©a et Courreur'),
(24, 5, '2016-04-20', NULL, 'Courreur', 'Petit mais court vite'),
(25, 5, '2016-04-06', 'F', 'LÃ©a', NULL),
(26, 6, '2011-01-11', 'F', 'HÃ©lÃ©na', NULL),
(27, 6, '1979-04-24', 'M', 'Marco', NULL),
(28, 6, '2016-03-12', NULL, 'Finaud', NULL),
(29, 6, '1970-05-05', 'M', 'Molosse', 'Entaille sur le flanc droit'),
(30, 7, '1997-03-14', 'M', 'Grand', NULL),
(31, 7, '2004-01-29', 'M', 'Fin', NULL),
(32, 7, '2009-06-28', 'F', 'Grande', NULL),
(33, 7, '2008-11-15', 'F', 'Ã‰lancÃ©e', 'Boite'),
(34, 8, '2004-10-11', 'M', 'Pato', NULL),
(35, 8, '1988-11-08', 'F', 'Baronne', NULL),
(36, 9, '2016-02-18', 'M', 'Dixon', NULL),
(37, 9, '1999-04-05', 'M', 'Seigneur', 'Male alpha'),
(38, 9, '2006-02-24', 'M', 'Vengeur', NULL),
(39, 9, '2016-04-09', NULL, 'Noiraud', 'FrÃ¨re ou sÅ“ur de Blanco'),
(40, 9, '2016-04-09', NULL, 'Blanco', 'FrÃ¨re ou sÅ“ur de Noiraud'),
(41, 9, '2012-06-07', 'F', 'Belle', 'Femelle alpha'),
(42, 10, '1997-06-15', 'M', 'Chef', NULL),
(43, 10, '2015-04-21', 'M', 'Pongo', 'Fratrie avec Parker et Fraisa'),
(44, 10, '2015-04-21', 'M', 'Parker', 'Fratrie avec Pongo et Fraisa'),
(45, 10, '2015-04-21', 'F', 'Fraisa', 'Fratrie avec Pongo et Parker'),
(46, 10, '2016-02-23', NULL, 'Vif', NULL),
(47, 10, '2006-07-14', 'F', 'Mam', NULL),
(48, 10, '2015-09-27', 'F', 'SÃ©verine', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
