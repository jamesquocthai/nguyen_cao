-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 04, 2022 at 09:57 AM
-- Server version: 5.7.36
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zoo`
--

-- --------------------------------------------------------

--
-- Table structure for table `personnels`
--

DROP TABLE IF EXISTS `personnels`;
CREATE TABLE IF NOT EXISTS `personnels` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom` varchar(40) NOT NULL,
  `prenom` varchar(40) NOT NULL,
  `date_nais` date DEFAULT NULL,
  `sexe` enum('M','F') NOT NULL,
  `fonction` varchar(30) NOT NULL,
  `salaire` decimal(7,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `personnels`
--

INSERT INTO `personnels` (`id`, `nom`, `prenom`, `date_nais`, `sexe`, `fonction`, `salaire`) VALUES
(1, 'Park', 'Bob', '1966-10-21', 'M', 'Directeur', '12512.50'),
(2, 'Park', 'HÃ©lÃ¨ne', '1970-04-07', 'F', 'SecrÃ©taire', '8984.23'),
(3, 'Sauvadet', 'Paul', '1981-05-17', 'M', 'Gardien', '2675.50'),
(4, 'Remon', 'GÃ©rard', '1986-11-09', 'M', 'Gardien', '2356.45'),
(5, 'Denton', 'Luc', '1971-11-07', 'M', 'Gardien', '3215.00'),
(6, 'Denton', 'Michelle', '1974-06-23', 'F', 'Gardien', '3075.00'),
(7, 'Park', 'Cindy', '1995-07-07', 'F', 'Gardien', '1970.89'),
(8, 'Park', 'Marc', '1994-01-17', 'M', 'Soigneur', '7502.10'),
(9, 'Moineau', 'Pauline', '1980-05-02', 'F', 'Soigneur', '10541.25'),
(10, 'Gretan', 'Ulysse', '1990-03-21', 'M', 'Soigneur', '8153.12'),
(11, 'Martin', 'Antoine', '1995-07-11', 'M', 'Livreur', '1950.60'),
(12, 'Griffit', 'MÃ©lanie', '1996-04-03', 'F', 'Livreur', '1950.60');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
