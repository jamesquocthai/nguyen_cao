-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 04, 2022 at 09:58 AM
-- Server version: 5.7.36
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zoo`
--

-- --------------------------------------------------------

--
-- Table structure for table `soigneurs`
--

DROP TABLE IF EXISTS `soigneurs`;
CREATE TABLE IF NOT EXISTS `soigneurs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pers` int(10) UNSIGNED NOT NULL,
  `race` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soigneurs`
--

INSERT INTO `soigneurs` (`id`, `pers`, `race`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 4),
(4, 3, 5),
(5, 3, 7),
(6, 4, 10),
(7, 6, 9),
(8, 6, 4),
(9, 7, 2),
(10, 8, 1),
(11, 8, 2),
(12, 8, 10),
(13, 9, 1),
(14, 9, 2),
(15, 9, 5),
(16, 9, 7),
(17, 9, 8),
(18, 10, 1),
(19, 10, 2),
(20, 10, 3),
(21, 10, 4),
(22, 10, 6),
(23, 10, 10);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
