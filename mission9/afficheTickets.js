$(document).ready(function () {
	$('#select1').change(function () {
		let id = $(this).val();
		let ok = $("#ok");
		$.ajax("./backticket.php",{
			data: "id="+id,
			type: "GET",
			success: function (response) {
				ok.html(response);
			}, error: function (response){
				ok.html("ERREUR !");
				console.log(response);
			}
		});
	});

	$('#resolved').click(function () {
		let id = $("#idTicket").html();
		let ok = $("#maj");
		$.ajax("./backticket.php",{
			data: "id="+id,
			type: "POST",
			success: function (response) {
				if (response=="") {
					ok.html("Resolved");
				}
			}, error: function (response){
				ok.html("ERREUR !");
				console.log(response);
			}
		});
	});
});