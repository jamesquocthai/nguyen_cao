<?php 

include("initDB.php");

//mission 9
require 'vendor/autoload.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
$app = new \Slim\App;

$app->get('/zaza', function(Request $request, Response $response){  
  return "wazaaaaaaaa";
});
$app->get('/bonjour', function(Request $request, Response $response){  
  return "bonjour";
});


$app->get('/animal/{id}', function(Request $request, Response $response){
  $id = $request->getAttribute('id');
       return getPersonnage($id);
});

$app->run();

creatBas();
rempBas();


?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Mon zoo Maplezoo</title>
    <link rel='stylesheet' type='text/css' href='flexbox.css' media='all'>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="function.js"></script>
    <script src="functionSlim.js"></script>
  </head>
  <body>
    <header>
      <a href="index.html"><img src="img/maplezooo.png" alt="Mon zoo Maple"></a>
      <ul class="menu">
        <li><a href="http://cyleo.alwaysdata.net/mission9/">Homepage</a></li>
        <li><a href="#s1">Normal Monster</a></li>
        <li><a href="#s2">Area Bosses</a></li>
        <li><a href="#s3">Major Bosses</a></li>
        <li><a href="formTicket.html">Report a problem</a></li>
        <li><a href="authentification.html">Connection</a></li>
        <li><a href="inscription.php">Register</a></li>
        <li><a href="./indeex.php/zaza">"Route zaza"</a></li>
        <li><a id="btn-valide">"Route bonjour"</a></li>
      </ul>
    </header>
      <section id="s1">
        <header>
          <h3>Normal Monsters Area</h3>
          <p>Updated by jamesthai</p>
        </header>
        <footer>
          <div class="cards">
            <div class="card">
              <header><h2>Orange Mushroom</h2></header>     
              <img class="jolie" src="img/OrangeMushroom.png" alt="OrangeMushroom">
              <div class="content">  
                <p>Monster's Stats</p>
              </div>
              <footer>
                <nav>
                 <a href="https://maplestory.fandom.com/wiki/Orange_Mushroom#Maple_Island">Click for more details</a>
                </nav>
              </footer>
            </div>
            <div class="card">
              <header><h2>Slime</h2></header>     
              <img class="jolie" src="img/Slime.jpg" alt="Slime">
              <div class="content">  
                <p>Monster's Stats</p>
              </div>
              <footer>
                <nav>
                 <a href="https://maplestory.fandom.com/wiki/Slime#Maple_Island">Click for more details</a>
                </nav>
              </footer>
            </div>
          </div>
        </footer>
      </section>

      <section id="s2">
        <header>
          <h3>Normal Bosses Area</h3>
          <p>Updated by jamesthai</p>
        </header>
        <footer>
          <div class="cards">
            <div class="card">
              <header><h2>Pink Bean</h2></header>     
              <img class="jolie" src="img/PinkBean.jpg" alt="Pink Bean">
              <div class="content">  
                <p>Monster's Stats</p>
              </div>
              <footer>
                <nav>
                 <a href="https://maplestory.fandom.com/wiki/Pink_Bean">Click for more details</a>
                </nav>
              </footer>
            </div>
            <div class="card">
              <header><h2>Horntail</h2></header>     
              <img class="jolie" src="img/Horntail.jpg" alt="Horntail">
              <div class="content">  
                <p>Monster's Stats</p>
              </div>
              <footer>
                <nav>
                 <a href="https://maplestory.fandom.com/wiki/Horntail">Click for more details</a>
                </nav>
              </footer>
            </div>
          </div>
        </footer>
      </section>

      <section id="s3">
        <header>
          <h3>Major Bosses Area</h3>
          <p>Updated by jamesthai</p>
        </header>
        <footer>
          <div class="cards">
            <div class="card">
              <header><h2>Damien</h2></header>     
              <img class="jolie" src="img/Damien.png" alt="Damien">
              <div class="content">  
                <p>Monster's Stats</p>
              </div>
              <footer>
                <nav>
                 <a href="https://maplestory.fandom.com/wiki/Damien/Monster">Click for more details</a>
                </nav>
              </footer>
            </div>
            <div class="card">
              <header><h2>Lotus</h2></header>     
              <img class="jolie" src="img/Lotus.jpg" alt="Lotus">
              <div class="content">  
                <p>Monster's Stats</p>
              </div>
              <footer>
                <nav>
                 <a href="https://maplestory.fandom.com/wiki/Lotus/Monster">Click for more details</a>
                </nav>
              </footer>
            </div>
          </div>
        </footer>
      </section>
      
      <div>
        <label for="idx">Entrez l'identifiant de l'animal dont vous souhaitez connaître les informations :</label>
        <input id="idx" type="number">
        <button id="btn-new-liste">Afficher les informations</button>
        <span id="result"></span>
      </div>
      
      
  </body>
</html>