<?php
function connectDB() {
    $conn = "";
    $server='mysql:dbname=zoo;host=127.0.0.1';
    $username = "root";
    $password = "";
    
    try {
        $conn = new PDO($server, $username, $password);   
    } catch (PDOException $e) {
        try {
            $conn = new PDO('mysql:dbname=cyleo_zoo;host=mysql-cyleo.alwaysdata.net', "cyleo", "mdpServer&C0M");
        } catch (PDOException $e) {
            echo "Connexion échouée\n" . $e->getMessage();
        }
    }
    return($conn);
}

function creatBas($bdd=null) { //création de la base de donnée si elle n'existe pas encore
    try {
        if ($bdd==null) {
            $bdd = connectDB();
        }
        $bdd->query(file_get_contents('zootickoon-master/creation_db_zoo.sql'));
        //$bdd->query(file_get_contents('data.sql'));
    } catch(T_STRING $e) {
        var_dump($e);
    } 
}

function rempBas($bdd=null) {
     try {
        if ($bdd==null) {
            $bdd = connectDB();
        }
        $bdd->query(file_get_contents('mesCSV/animaux.sql'));
        $bdd->query(file_get_contents('mesCSV/enclos.sql'));
        $bdd->query(file_get_contents('mesCSV/loc_animaux.sql'));
        $bdd->query(file_get_contents('mesCSV/personnels.sql'));
        $bdd->query(file_get_contents('mesCSV/races.sql'));
        $bdd->query(file_get_contents('mesCSV/soigneurs.sql'));
    } catch(T_STRING $e) {
        var_dump($e);
    }
}


// non fonctionnel, erreur mémoire ? variable perdue ?
function recupCSV ($name, $table) {
    $db=connectDB();
    $sql = "";
    $sql .= "INSERT INTO :table (id";
    $text = "";
    $null=NULL;
    $i = 0;
    $line = array();
    $datas = file_get_contents($name);
    $datas = explode(PHP_EOL, $datas, 2);
    $line = explode(';', $datas[0]);
    for ($i=0; $i<count($line); $i++) {
        $sql = $sql.",".$line[$i];
    }
    $sql = $sql.") VALUES (";
    $i = 0;
    while ((isset($datas[1])) && ($datas[1]!=null)) {
        $j=0;
        $text = $sql;
        $datas = explode(PHP_EOL, $datas[1], 2);
        $line = explode(';', $datas[0]);
        $text = $text.$i;
        for ($j=0; $j<count($line); $j++) {
            $text = $text.",:".$j;
        }
        $text = $text.");";
        //echo $text;
        $results=$db->prepare($text);
        $results->bindParam(":table", $table);
        for ($j=0; $j<count($line); $j++) {
            if ($line[$j]!="NULL") {
                $results->bindParam(":".$j, $line[$j]);
            } else {
                $results->bindParam(":".$j, $null);
            }
        }
        $bool=$results->execute();
        sleep(1);
        $i++;
    }
    return($line);
}

function userTableExist($bdd=null) {
    if ($bdd==null) {
        $bdd = connectDB();
    }
    $sql = "SELECT * FROM user;";
    $results = $bdd->prepare($sql);
    try {
        $results->execute();
        if ($results->fetch(PDO::FETCH_ASSOC)!=null) {
            return (1);
        }
    } catch(T_STRING $e) {
        return(0);
    }
    return(0);
}
 
//mission 9
function getPersonnage($id)
{
  $sql = "SELECT * FROM animaux WHERE id=:id";
  try{
    $dbh=connectDB();
    $statement = $dbh->prepare($sql);
    $statement->bindParam(":id",$id);
    $statement->execute();
     $result = $statement->fetchAll(PDO::FETCH_CLASS); 
                 return json_encode($result, JSON_PRETTY_PRINT);
  } catch(PDOException $e){
    return '{"error":'.$e->getMessage().'}';
  }
}

?>