<?php
	require("recupTicket.php");
	$conn=connectDB();
	if ($conn){
		if (isset($_POST["submit"])){			
			$login=$_POST["login"];	
			$sujet=$_POST["sujet"];
			$description=$_POST["description"];
			$id=$_POST['id'];
			$prio=$_POST["prio"];
			$secteur=$_POST["secteur"];
			$statut=$_POST["statut"];
			//Set current time
			$datet = date("Y-m-d H:i:s");
			if ($prio>=0 && $prio<=3){
				$urgence="faible";
			}
			else if ($prio>=4 && $prio<=7){
				$urgence="moyen";
			}
			else{
				$urgence="eleve";
			}
			$sql="UPDATE ticket SET login=:login,sujet=:sujet,description=:description,datet=:datet,prio=:urgence,secteur=:secteur,statut=:statut WHERE id=:id";
			$envoyee=$conn->prepare($sql);
			$envoyee->bindParam(':login', $login);
            $envoyee->bindParam(':sujet', $sujet);
            $envoyee->bindParam(':description', $description);
            $envoyee->bindParam(':datet', $datet);
            $envoyee->bindParam(':urgence', $urgence);
            $envoyee->bindParam(':secteur', $secteur);
            $envoyee->bindParam(':statut', $statut);
            $envoyee->bindParam(':id', $id);
			$envoyee->execute();


			if($envoyee){
				header('location:afficheListeTickets.php');
			}
			else{
				echo "Something is wrong!";
			}
		}
	}
	else{
		echo "Something went really really wrong!";
	}
?>
			
<!DOCTYPE html> 
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Edit Ticket</title>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
		<link rel="stylesheet" href="ticket.css">
	</head>

	<body>
		<div class="body">
			<h1>
				Edit ticket form
			</h1>
			<form method="POST" action="<?php $_PHP_SELF?>" class="border rounded">
				<?php
					if (isset($_GET['modif'])){
						$id = $_GET['modif'];
						$select="SELECT * FROM ticket WHERE id = $id";
						$query=$conn->query($select);
						while($data=$query->fetch(PDO::FETCH_ASSOC))
						{
				?>
				<div class="login margin-1">
					<h2>Connection</h2>
	  				<div class="form-group">
						<label for="exampleInputEmail1">Email address</label>
						<input type="email"  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="login" placeholder="Enter email" value="<?php echo $data['login'];?>">
						<small id="email" class="form-text text-muted">We'll never share your email with anyone else.</small>
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Password</label>
						<input type="password" class="form-control" id="exampleInputPassword1" required placeholder="Password">
					</div>
				</div>

				<div class="ticket margin-1">
					<h2>Ticket</h2>

					<div class="form-group">
						<label for="titleTicket">Name your incident ticket</label>
						<input type="text" name= "sujet" class="form-control" id="titleTicket" required placeholder="Pink Bean escaped!" value="<?php echo $data['sujet'];?>">
					</div>

					<div class="form-group">
						<label for="emergencyLvl-list" class="form-label">Emergency level</label>
						<ul id="emergencyLvl-list" class="list-group list-group-horizontal-sm">
							<li class="list-group-item">0 : It's OK, it can wait.</li>
							<li class="list-group-item range-middle">	
								<input type="range" name= "prio" class="form-range" id="emergencyLvl" value="4" max="10" min="0" step="1" list="oklvl" aria-describedby="rangeHelp1" required>
								<datalist id="oklvl">
									<option value="0" label="It's OK, it can wait.">
									<option value="10" label="APOCALYPSE IS ON ITS WAY!!!">
								</datalist>
							</li>
							<li class="list-group-item">10 : APOCALYPSE IS ON ITS WAY!!!</li>
						</ul>
					</div>

					<div class="form-group">
						<label for="descripTicket" name="description">Description of the incident</label>
						<input name= "description" class="form-control" id="descripTicket" required placeholder="Pink Bean flied above the fences with kids' balloons!" value="<?php echo $data['description'];?>">
					</div>

					<div class="form-group">
						<label for="statusTicket">Status</label>
						<input type="text"  class="form-control" id="statusTicket" required placeholder="ongoing?" name="statut" value="<?php echo $data['statut'];?>">
					</div>

					<span>Zoo sector</span>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="secteur" id="Radio1" value="Normal mob" checked>
  						<label class="form-check-label" for="Radio1">
    						Normal monsters zone
  						</label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="secteur" id="Radio2" value="Area boss">
						<label class="form-check-label" for="Radio2">
						    Area bosses zone
						</label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="secteur" id="Radio3" value="Major boss">
						<label class="form-check-label" for="Radio3">
						    Major bosses zone
						</label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="secteur" id="Radio4" value="Legendary">
						<label class="form-check-label" for="Radio4">
						    Legendary bosses zone
						</label>
					</div>
					<input type="hidden" name="id" value="<?php echo $data['id'];?>">
				</div>
				<button type="submit" name="submit" class="btn btn-primary margin-1">Submit</button>
			   	<a href="afficheListeTickets.php" class="btn btn-success margin-1">List of tickets</a>
				<?php
						}
					}
					//modifierTicket();
				?>
			</form>
		</div>
	</body>
</html>