<?php 
echo "Hello"; 
$heure = date("H");
echo $heure;
if ($heure < "12"){
  echo '<img src="img/Zebra.png" alt="zebra">';
}
elseif ($heure > "12"){
  echo '<img src="img/giraffe.jpg" alt="giraffe">';
}
else{
  echo '<img src="img/panda.jpg" alt="panda">';
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Mon zoo Maplezoo</title>
    <link rel='stylesheet' type='text/css' href='flexbox.css' media='all'>
    <script src="function.js"></script>
  </head>
  <body>
    <header>
      <a href="index.php"><img src="img/maplezooo.png" alt="Mon zoo Maple"></a>
      <ul class="menu">
        <li><a href="../mission1/wordpress/">Homepage</a></li>
        <li><a href="#s1">Normal Monster</a></li>
        <li><a href="#s2">Area Bosses</a></li>
        <li><a href="#s3">Major Bosses</a></li>
        <li><a href="formTicket.php">Report a problem</a></li>
        <li><a href="authentification.html">Connection</a></li>
        <li><a href="inscription.php">Register</a></li>
      </ul>
    </header>
      <section id="s1">
        <header>
          <h3>Normal Monsters Area</h3>
          <p>Updated by jamesthai</p>
        </header>
        <footer>
          <div class="cards">
            <div class="card">
              <header><h2>Orange Mushroom</h2></header>     
              <img class="jolie" src="img/OrangeMushroom.png" alt="OrangeMushroom">
              <div class="content">  
                <p>Monster's Stats</p>
              </div>
              <footer>
                <nav>
                 <a href="https://maplestory.fandom.com/wiki/Orange_Mushroom#Maple_Island">Click for more details</a>
                </nav>
              </footer>
            </div>
            <div class="card">
              <header><h2>Slime</h2></header>     
              <img class="jolie" src="img/Slime.jpg" alt="Slime">
              <div class="content">  
                <p>Monster's Stats</p>
              </div>
              <footer>
                <nav>
                 <a href="https://maplestory.fandom.com/wiki/Slime#Maple_Island">Click for more details</a>
                </nav>
              </footer>
            </div>
          </div>
        </footer>
      </section>

      <section id="s2">
        <header>
          <h3>Normal Bosses Area</h3>
          <p>Updated by jamesthai</p>
        </header>
        <footer>
          <div class="cards">
            <div class="card">
              <header><h2>Pink Bean</h2></header>     
              <img class="jolie" src="img/PinkBean.jpg" alt="Pink Bean">
              <div class="content">  
                <p>Monster's Stats</p>
              </div>
              <footer>
                <nav>
                 <a href="https://maplestory.fandom.com/wiki/Pink_Bean">Click for more details</a>
                </nav>
              </footer>
            </div>
            <div class="card">
              <header><h2>Horntail</h2></header>     
              <img class="jolie" src="img/Horntail.jpg" alt="Horntail">
              <div class="content">  
                <p>Monster's Stats</p>
              </div>
              <footer>
                <nav>
                 <a href="https://maplestory.fandom.com/wiki/Horntail">Click for more details</a>
                </nav>
              </footer>
            </div>
          </div>
        </footer>
      </section>

      <section id="s3">
        <header>
          <h3>Major Bosses Area</h3>
          <p>Updated by jamesthai</p>
        </header>
        <footer>
          <div class="cards">
            <div class="card">
              <header><h2>Damien</h2></header>     
              <img class="jolie" src="img/Damien.png" alt="Damien">
              <div class="content">  
                <p>Monster's Stats</p>
              </div>
              <footer>
                <nav>
                 <a href="https://maplestory.fandom.com/wiki/Damien/Monster">Click for more details</a>
                </nav>
              </footer>
            </div>
            <div class="card">
              <header><h2>Lotus</h2></header>     
              <img class="jolie" src="img/Lotus.jpg" alt="Lotus">
              <div class="content">  
                <p>Monster's Stats</p>
              </div>
              <footer>
                <nav>
                 <a href="https://maplestory.fandom.com/wiki/Lotus/Monster">Click for more details</a>
                </nav>
              </footer>
            </div>
          </div>
        </footer>
      </section>
  </body>
</html>