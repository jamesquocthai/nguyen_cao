<?php

function connectDB() {
    $conn = "";
    $server='mysql:dbname=zootickoon;host=127.0.0.1';
    $username = "root";
    $password = "";
    
    try {
        $conn = new PDO($server, $username, $password);   
    } catch (PDOException $e) {
        try {
            $conn = new PDO('mysql:dbname=cyleo_zoo;host=mysql-cyleo.alwaysdata.net', "cyleo", "mdpServer&C0M");
            $alwaysdata = true;
        } catch (PDOException $e) {
            $alwaysdata = false;
            echo "Connexion échouée\n" . $e->getMessage();
        }
    }
    return($conn);
}

function createBaseUser($bdd=null) { //création de la base de donnée 
    $bdd = connectDB();
    $bdd->query(file_get_contents('user.sql')); //on appelle le fichier user.sql qui contient des intstructions en SQL
}

function userTableExist($bdd=null) {
    if ($bdd==null) {
        $bdd = connectDB();
    }
    $sql = "SELECT * FROM user;";
    $results = $bdd->prepare($sql);
    try {
        $results->execute();
        if ($results->fetch(PDO::FETCH_ASSOC)!=null) {
            return (1);
        }
    } catch(T_STRING $e) {
        return(0);
    }
    return(0);
}
function userExist($mail) {
    if (!userTableExist()) { //on vérifie l'existence d'une base de données. Si il n'y en a pas, on essaie de la créer.
        createBaseUser();
    }
    if (userTableExist()) {
        $db=connectDB();
        $sql = "SELECT count(id) FROM user WHERE email=:email;";
        $results=$db->prepare($sql);
        $results->bindParam(":email", $mail);
        $results->execute();
        $user=$results->fetch(PDO::FETCH_ASSOC);
        return($user["count(id)"]);
    }
    return(0);
}

function register($mail, $password) {
    if (userExist($mail)<1) {
        $db=connectDB();
        $sql = "INSERT INTO user(id,email,password,registration) VALUES (NULL, :email, :password, :date);";
        $results=$db->prepare($sql);
        $date=date("Y-m-d H:i:s");
        $results->bindParam(":email", $mail);
        $results->bindParam(":password", $password);
        $results->bindParam(":date", $date);
        $bool=$results->execute();
        if (userExist($mail)>=1) {
            return(1);
        }
        return($bool);
    }
    return(0);
}
?>