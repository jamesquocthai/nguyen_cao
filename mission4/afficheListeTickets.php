<?php 
	require("recupTicket.php");
	creatBaseTicket();
	$conn=connectDB();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>List of tickets</title>
</head>
<body>
<div class="container">
<table class="table table-bordered">
	<h1>List of tickets</h1>
	<thead>
		<tr>
			<td bgcolor="#E6E6FA">ID</td>
			<td bgcolor="#E6E6FA">Date</td>
			<td bgcolor="#E6E6FA">Login</td>
			<td bgcolor="#E6E6FA">Subject</td>
			<td bgcolor="#E6E6FA">Description</td>
			<td bgcolor="#E6E6FA">Priority</td>
			<td bgcolor="#E6E6FA">Zoo sector</td>
			<td bgcolor="#E6E6FA">Status</td>
			<td bgcolor="#E6E6FA">Your action</td>
		</tr>
	</thead>
	<tbody>
	
	<?php 
		$select="SELECT * FROM ticket";
		$query=$conn->query($select);
		while($data=$query->fetch(PDO::FETCH_ASSOC))
		{
	?>
		<tr>
			<td><?php echo $data["id"];?></td>
			<td><?php echo $data["datet"];?></td>
			<td><?php echo $data["login"];?></td>
			<td><?php echo $data["sujet"];?></td>
			<td><?php echo $data["description"];?></td>
			<td><?php echo $data["prio"];?></td>
			<td><?php echo $data["secteur"];?></td>
			<td><?php echo $data["statut"];?></td>
			<td><a href="afficheTickets.php?view=<?php echo $data["id"];?>" class="btn btn-success">View this ticket</td>
		</tr>
	<?php
		}
	?>
	</tbody>
</table>
</div>
</body>
</html>