<!DOCTYPE html> 
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Tickets</title>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
		<link rel="stylesheet" href="ticket.css">
	</head>

	<body class="body">
<?php
function connectDB() {
    $conn = "";
    $server='mysql:dbname=zootickoon;host=127.0.0.1';
    $username = "root";
    $password = "";
    
    try {
        $conn = new PDO($server, $username, $password);   
    } catch (PDOException $e) {
        try {
            $conn = new PDO('mysql:dbname=cyleo_zoo;host=mysql-cyleo.alwaysdata.net', "cyleo", "mdpServer&C0M");
        } catch (PDOException $e) {
            echo "Connexion échouée\n" . $e->getMessage();
        }
    }
    return($conn);
}

function creatBaseTicket() { //création de la base de donnée si elle n'existe pas encore
    $bdd = connectDB();    
    $bdd->query(file_get_contents('bd.sql'));
}

function recupererTicket(){
	$conn=connectDB();
	if (isset($_POST["submit"])){
		$login=$_POST["login"];	
		$sujet=$_POST["sujet"];
		$description=$_POST["description"];
		$prio=$_POST["prio"];
		$secteur=$_POST["secteur"];
		$statut=$_POST["statut"];
		//Set current time
		$datet = date("Y/m/d H:i:s");
		if ($prio>=0 && $prio<=3){
			$urgence="faible";
		}
		else if ($prio>=4 && $prio<=7){
			$urgence="moyen";
		}
		else{
			$urgence="eleve";
		}
		$sql="INSERT Into ticket(login,sujet,description,prio,secteur,datet,statut) VALUES (:login, :sujet, :description, :urgence, :secteur, :datet, :statut)";
		$envoyee=$conn->prepare($sql);
		$envoyee->bindParam(':login', $login);
        $envoyee->bindParam(':sujet', $sujet);
        $envoyee->bindParam(':description', $description);
        $envoyee->bindParam(':datet', $datet);
        $envoyee->bindParam(':urgence', $urgence);
        $envoyee->bindParam(':secteur', $secteur);
        $envoyee->bindParam(':statut', $statut);
		$envoyee->execute();
		if($envoyee){
			echo "Ticket sent";
		}
		else{
			echo "Something is wrong!";
		}
	}
}

?>

	</body>
</html>
